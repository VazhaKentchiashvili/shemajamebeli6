package com.vazhasapp.shemajamebeli6.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class User(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    val title: String?,
    val description: String?,
    val icon: String?,
) {
    constructor(
        title: String?,
        description: String?,
        icon: String?,
    ) : this(0, title, description, icon)
}
