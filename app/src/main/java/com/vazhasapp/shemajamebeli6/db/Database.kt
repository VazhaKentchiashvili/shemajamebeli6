package com.vazhasapp.shemajamebeli6.db

import androidx.room.Room
import com.vazhasapp.shemajamebeli6.App

object Database : App() {
    val db: UserDatabase by lazy {
        Room.databaseBuilder(
            context!!,
            UserDatabase::class.java,
            "user_db").build()
    }
}
