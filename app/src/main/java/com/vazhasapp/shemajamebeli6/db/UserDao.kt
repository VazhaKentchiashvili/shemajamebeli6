package com.vazhasapp.shemajamebeli6.db

import androidx.room.*

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: User)

    @Query("SELECT * FROM user_table ORDER BY id ASC")
    suspend fun getAllUser(): MutableList<User>

    @Query("UPDATE user_table SET title= :updatedTitle, description= :updatedDescription, icon= :updatedIcon")
    suspend fun updateUser(updatedTitle: String, updatedDescription: String, updatedIcon: String)

    @Delete
    suspend fun deleteUser(user: User)

}