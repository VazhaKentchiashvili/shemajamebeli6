package com.vazhasapp.shemajamebeli6.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.shemajamebeli6.adapter.UserRecyclerAdapter
import com.vazhasapp.shemajamebeli6.databinding.FragmentUserListBinding
import com.vazhasapp.shemajamebeli6.viewModel.UserViewModel

class UserListFragment : Fragment() {

    private var _binding: FragmentUserListBinding? = null
    private val binding get() = _binding!!

    private val myAdapter = UserRecyclerAdapter()

    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        setupRecyclerView()
        listeners()
    }

    private fun listeners() {
        userViewModel.user.observe(viewLifecycleOwner, {
            myAdapter.setData(it)
        })
    }

    private fun setupRecyclerView() {
        binding.userRecyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}