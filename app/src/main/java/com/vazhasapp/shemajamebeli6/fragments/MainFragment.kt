package com.vazhasapp.shemajamebeli6.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vazhasapp.shemajamebeli6.R
import com.vazhasapp.shemajamebeli6.databinding.FragmentMainBinding
import com.vazhasapp.shemajamebeli6.db.User
import com.vazhasapp.shemajamebeli6.viewModel.UserViewModel

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val userViewModel: UserViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        binding.btnAddUser.setOnClickListener {
            enteredTextCheck()
        }

        binding.btnShowUsers.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_userListFragment)
        }
    }

    private fun enteredTextCheck() {

        if (binding.etTitle.text.length in 6..29 &&
            binding.etDescription.text.length in 33..300
        ) {
            Toast.makeText(requireContext(), "User Added", Toast.LENGTH_SHORT).show()
            userViewModel.write(getEnteredUser())
        } else {
            Toast.makeText(
                requireContext(),
                "Title min symbols: 5 max 32, description: 30 and 300",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun getEnteredUser(): User {
        val title = binding.etTitle.text.toString()
        val description = binding.etDescription.text.toString()
        val image = binding.etIcon.text.toString()

        return User(0,title, description, image)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}