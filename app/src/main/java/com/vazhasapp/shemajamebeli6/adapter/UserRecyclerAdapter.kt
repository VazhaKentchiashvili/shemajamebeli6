package com.vazhasapp.shemajamebeli6.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vazhasapp.shemajamebeli6.databinding.UserCardViewBinding
import com.vazhasapp.shemajamebeli6.model.User

class UserRecyclerAdapter : RecyclerView.Adapter<UserRecyclerAdapter.UserRecyclerViewHolder>() {

    private val userList = mutableListOf<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UserRecyclerViewHolder(
            UserCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: UserRecyclerViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = userList.size

    inner class UserRecyclerViewHolder(private val binding: UserCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var currentUser: User

        fun bind() {
            binding.tvTitle.text = currentUser.title
            binding.tvDescription.text = currentUser.description
            Glide.with(binding.imUserIcon.context).load(currentUser.icon).into(binding.imUserIcon)
        }
    }

    fun setData(enteredUserList: MutableList<User>) {
        userList.clear()
        userList.addAll(enteredUserList)
        notifyDataSetChanged()
    }
}