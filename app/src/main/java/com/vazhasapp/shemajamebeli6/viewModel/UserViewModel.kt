package com.vazhasapp.shemajamebeli6.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.shemajamebeli6.db.Database.db
import com.vazhasapp.shemajamebeli6.db.User
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class  UserViewModel : ViewModel(){

    private val _user = MutableLiveData<MutableList<User>>()
    val user: LiveData<MutableList<User>> = _user

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    fun write(
        user: User
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                db.userDao()
                    .insertUser(user)
            }
        }
    }

    fun read() {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                _user.postValue(db.userDao().getAllUser())
            }
        }
    }

    fun update(updatedTitle: String, updatedDescription: String, updatedIcon: String) {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                db.userDao().updateUser(updatedTitle, updatedDescription, updatedIcon)
            }
        }
    }

    fun deleteUser(user: User) {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                db.userDao().deleteUser(user)
            }
        }
    }
}