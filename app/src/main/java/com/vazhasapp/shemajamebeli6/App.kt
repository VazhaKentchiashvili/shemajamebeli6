package com.vazhasapp.shemajamebeli6

import android.app.Application
import android.content.Context

open class App : Application() {

    override fun onCreate() {
        super.onCreate()

        context = this
    }

    companion object {
        var context: Context? = null
    }
}