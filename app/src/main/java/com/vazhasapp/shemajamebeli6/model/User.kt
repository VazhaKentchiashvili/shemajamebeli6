package com.vazhasapp.shemajamebeli6.model

data class User(
    val title: String,
    val description: String,
    val icon: String,
)